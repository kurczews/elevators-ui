# elevators-ui

[React](https://reactjs.org/) based GUI for [elevators challenge](https://bitbucket.org/kurczews/elevators/src/master/)

## Running

Run `npm start` and visit [http://localhost:3000](http://localhost:3000)

## Additional info

Project root structure created via [create-react-app](https://create-react-app.dev/). See original [README](README.react.md) for details.

P.S. I hadn't writing in JS since times when JQuery was only one javascript framework and PHP was a thing.