import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import InfoBox from './infoBox.js';
import DataSource from './dataSource.js';

const EMPTY_SLOT = ' ';

// FIXME warning each children should have unique "key" prop

var dataSource = new DataSource()

function ElevatorSlot(props) {
  return (
    <button className="elevator-slot" style={props.style}>
      {props.value}
    </button>
  );
}

function ElevatorButton(props) {
  return (
    <button className="elevator-button"
            onClick={ () => props.elevatorBtnHandler(props.floorIndex) } >
      {props.floorIndex}
    </button>
  );
}

class Shaft extends React.Component {

  constructor(props) {
    super(props);
    let shaft = Array(this.props.floorsNumber);
    this.state = {
      shaft: shaft,
    }
  }

  renderElevatorSlot(floor, elevatorIndex) {
    var style;
    if (elevatorIndex === this.state.selectedElevator) style = {background: '#b4ecb4'};
    if (elevatorIndex === this.state.selectedElevator && floor === this.state.selectedFloor) style = {background: '#32cd32'};

    return <ElevatorSlot value={this.state.shaft[floor][elevatorIndex]} style={style} />;
  }

  renderFloor(elevators, floorIndex) {
    return (
      <div className="floor">
        <ElevatorButton floorIndex={this.calculateFloorIndex(floorIndex)} elevatorBtnHandler={(floor) => this.handleElevatorRequest(floor)}/>
        { elevators.map((_, elevatorIndex) => this.renderElevatorSlot(floorIndex, elevatorIndex)) }
      </div>
    );
  }

  render() {
    const shaft = this.state.shaft.map((elevators, floorIndex) => this.renderFloor(elevators, floorIndex));

    return (
      <div>
        {shaft}
        <InfoBox />
      </div>
    );
  }

  handleElevatorRequest(floor) {
    dataSource.requestElevator(floor, (elevator) => {
      this.setState({
        selectedElevator: elevator.id,
        selectedFloor: this.calculateFloorIndex(floor),
      });
    });
  }

  handleShaftState(elevators) {
    var shaft = newShaft(this.props.floorsNumber, elevators.length);
    for (let elevator of elevators) {
      shaft[this.calculateFloorIndex(elevator.currentFloor)][elevator.id] = elevator.direction.charAt(0);
    }
    this.setState({
      shaft: shaft,
    });

    function newShaft(floorsNumber, elevatorsNumber) {
      return Array(floorsNumber).fill(null).map(floor => Array(elevatorsNumber).fill(EMPTY_SLOT));
    }
  }

  handleElevatorEvent(data) {
    const shaft = this.state.shaft.slice();
    var elevator = JSON.parse(data);

    clearElevatorShaft(shaft, elevator.id);
    shaft[this.calculateFloorIndex(elevator.currentFloor)][elevator.id] = elevator.direction.charAt(0);

    this.setState({
      shaft: shaft,
    });

    function clearElevatorShaft(shaft, elevatorId) {
      for (let floor of shaft) {
        floor[elevatorId] = ' ';
      }
    };
  }

  calculateFloorIndex(floor) {
    return this.props.floorsNumber - floor - 1;
  }

  componentDidMount() {
    dataSource.pullActualState((data) => this.handleShaftState(data));
    dataSource.openSseChannel((data) => this.handleElevatorEvent(data));
  }

  componentWillUnmount() {
    dataSource.closeSseChannel();
  }
}

function App(props) {
  return dataSource.isHealthy() ? <Shaft floorsNumber={dataSource.getFloorsNumber()}/> : <span style={{color: "red"}}>Error: backend server is not ready, sorry ¯\_(ツ)_/¯</span>;
}

ReactDOM.render(
  (<div>
    <App />
  </div>),
  document.getElementById('root')
);