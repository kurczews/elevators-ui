const BACKEND_URL = 'http://localhost:8080';
const EVENT_STREAM_URL = BACKEND_URL + '/events';
const REQUEST_ELEVATOR_URL = BACKEND_URL + '/elevators/request/';
const LIST_ELEVATOR_URL = BACKEND_URL + '/elevators';
const FLOORS_NUMBER_URL = BACKEND_URL + '/elevators/floors';
const HEALTH_URL = BACKEND_URL + '/health';

class DataSource {

  eventSource;

  isHealthy() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', HEALTH_URL, false);
    try {
      xhr.send();
    } catch (e) {
      console.log(e);
      return false;
    }
    return xhr.status === 200;
  }

  getFloorsNumber() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', FLOORS_NUMBER_URL, false);
    try {
      xhr.send();
    } catch (e) {
      console.log(e);
      return false;
    }
    if (xhr.readyState !== xhr.DONE || xhr.status !== 200) {
      return null;
    }
    return parseInt(xhr.responseText)
  }

  requestElevator(floor, handler) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', `${REQUEST_ELEVATOR_URL}${floor}`, true);
    xhr.send();

    xhr.onreadystatechange = (event) => {
      if (xhr.readyState !== xhr.DONE || xhr.status !== 200) {
        return;
      }
      var elevator = JSON.parse(xhr.responseText);
      handler(elevator)
    }
  }

  pullActualState(handler) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', LIST_ELEVATOR_URL, true);
    xhr.send();

    xhr.onreadystatechange = (event) => {
      if (xhr.readyState !== xhr.DONE || xhr.status !== 200) {
        return;
      }
      var elevators = JSON.parse(xhr.responseText);
      handler(elevators);
    }
  }

  openSseChannel(handler) {
    if (!!window.EventSource) {
      this.eventSource = new EventSource(EVENT_STREAM_URL);
    } else {
      // TODO add polling fallback
      return;
    }
    this.eventSource.addEventListener('open', (event) => {
      console.log("Connection was opened.");
    });
    this.eventSource.addEventListener('message', (event) => {
      handler(event.data);
    });
    this.eventSource.addEventListener('error', (event) => {
      if (event.readyState === EventSource.CLOSED) {
        console.log("Connection was closed.");
      }
    });
  }

  closeSseChannel() {
    if (!!this.eventSource) {
      this.eventSource.close();
    }
  }
}

export default DataSource