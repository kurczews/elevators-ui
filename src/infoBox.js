import React from 'react';

class InfoBox extends React.Component {
  render() {
    return (
      <div>
        <p>
          Legend:
          <li>U - elevator is going up</li>
          <li>D - elevator is going down</li>
          <li>N - elevator is idle</li>
        </p>
        <p>Usage:</p>
        <p>
          Use yellow buttons to call elevator.<br/>
          Green marker shows elevator requested by hand.<br/><br/>

          You can run an automated simulation via <a href="localhost:8080/simulate" target="_blank" rel="noopener noreferrer">link</a>.
        </p>
      </div>
    );
  }
}

export default InfoBox;